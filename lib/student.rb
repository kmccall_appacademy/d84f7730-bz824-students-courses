class Student
  attr_accessor :first_name, :last_name
  attr_reader :courses, :name
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @name = @first_name + ' ' + @last_name
  end

  def enroll(new_course)
    conflicts = @courses.select {|course| new_course.conflicts_with? course}
    if conflicts.empty?
      @courses << new_course
      new_course.students << self
    else
      raise Exception
    end
  end

  def course_load
    res = Hash.new(0)
    @courses.each do |course|
      res[course.department] += course.credits
    end

    res
  end
end
